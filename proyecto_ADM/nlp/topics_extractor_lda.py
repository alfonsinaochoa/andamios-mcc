
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from nltk.corpus import stopwords
from nlp.topics_extractor_nmf import TopicsExtractorNMF


class TopicsExtractorLDA(TopicsExtractorNMF):
    def __init__(self, n_topics=5, n_top_words=10, language='english'):
        TopicsExtractorNMF.__init__(self,n_topics, n_top_words, language)


    def build_model(self, tfidf):
        lda = LatentDirichletAllocation(n_topics=self.n_topics, max_iter=5,
                                learning_method='online',
                                learning_offset=50.,
                                random_state=0)
        
        model = lda.fit(tfidf)
        return model
