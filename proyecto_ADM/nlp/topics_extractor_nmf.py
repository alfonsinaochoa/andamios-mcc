
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from nltk.corpus import stopwords


class TopicsExtractorNMF():
    def __init__(self, n_topics=5, n_top_words=10, language='english'):
        self.n_samples = 10
        self.n_features = 50
        self.n_topics = n_topics
        self.n_top_words = n_top_words
        self.language = language
        self.stopwords = stopwords.words(language)



    def build_model(self, tfidf):
        nmf = NMF(n_components=self.n_topics, 
            random_state=1, 
            alpha=.1, 
            l1_ratio=.5)
        model = nmf.fit(tfidf)
        return model

    def extract_topics(self, docs):
        self.n_samples = len(docs)
	print self.n_samples
        #self.n_topics = self.n_samples # we want one topic for each doc
        #if self.n_samples > 15:
        #    min_df = 0.1
        if self.n_samples > 5:
            min_df = 2
        else:
            min_df = 1
        max_df = 0.6
        
        if self.n_samples == 1:
            min_df = 0
            max_df = 1.0

        print("Extracting tf-idf features for NMF...")
        tfidf_vectorizer = TfidfVectorizer(
            max_df=max_df, 
            min_df=min_df,
            #max_features=self.n_features,
            #stop_words=self.language, # no stop words used, instead filter with max_df
            stop_words=self.stopwords,
            ngram_range = (1,3)
            #use_idf = False
            #strip_accents='ascii'
            )

        tfidf = tfidf_vectorizer.fit_transform(docs)

        model = self.build_model(tfidf)
        tfidf_feature_names = tfidf_vectorizer.get_feature_names()
        topics = {}

        for topic_idx, topic in enumerate(model.components_):  # @UnusedVariable
            #print("Topic #%d:" % topic_idx)
            #print(" ".join([feature_names[i] for i in topic.argsort()[:-n_top_words - 1:-1]]))
            words= [tfidf_feature_names[i] for i in topic.argsort()[:-self.n_top_words - 1:-1]]
            
            for w in words:
                topics[w]= 1 if w not in topics else topics[w]+1 
            #topics.append(words)

        return topics
    
