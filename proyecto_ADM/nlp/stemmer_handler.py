
import nltk
from nltk.stem import SnowballStemmer


class StemmerHandler():
    
    def __init__(self, language='english'):
        if language == 'english':
            self.stemmer = nltk.PorterStemmer()
        else:
            self.stemmer = SnowballStemmer(language)


    def stem_word(self, word):
        try:
            return self.stemmer.stem(word)
        except UnicodeDecodeError:
            return word 
