
class LexicalDiversity():

    def get_lexical_diversity(self, text):
        return len(set(text)) / float(len(text)) 