
from nltk.corpus import stopwords

class StopWordsHandler():
    def __init__(self, language='english'):
        self.stop = set(stopwords.words(language))

    def process_word(self, word):

        if len(word) < 3:
            return True

        return word in self.stop