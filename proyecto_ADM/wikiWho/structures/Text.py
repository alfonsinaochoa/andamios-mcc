'''
Created on Feb 20, 2013

@author: maribelacosta
'''
#import sys
#reload(sys)
#sys.setdefaultencoding('utf-8')
import hashlib
import unicodedata

def calculateHash(text):
    return hashlib.md5(text).hexdigest()


def splitIntoParagraphs(text):
    paragraphs = text.split("\n\n")

    return paragraphs

    
def splitIntoSentences(text):
    p = text

    p = p.replace('. ', '.@@@@')
    p = p.replace('\n', '\n@@@@')
    p = p.replace('; ', ';@@@@')
    p = p.replace('? ', '?@@@@')
    p = p.replace('! ', '!@@@@')
    #p = p.replace('.{', '.||{')
    #p = p.replace('!{', '!||{')
    #p = p.replace('?{', '?||{')
    p = p.replace('>{', '>@@@@{')
    p = p.replace('}<', '}@@@@<')
    #p = p.replace('.[', '.||[')
    #p = p.replace('.]]', '.]]||')
    #p = p.replace('![', '!||[')
    #p = p.replace('?[', '?||[')
    p = p.replace('<ref', '@@@@<ref')
    p = p.replace('/ref>', '/ref>@@@@')
    
    
    while '@@@@@@@@' in p :
        p = p.replace('@@@@@@@@', '@@@@')

    sentences = p.split('@@@@')
        
    return sentences

def elimina_tildes(cadena):
    s = ''.join((c for c in unicodedata.normalize('NFD',unicode(cadena)) if unicodedata.category(c) != 'Mn'))
    return s.decode()

def splitIntoWords(text):
    p = text
    #print 'antes de'
    #print p
    p = p.replace('|', '||@||')
            
    p = p.replace('<', '||<').replace('>', '>||')
    p = p.replace('?', '?||').replace('!', '!||').replace('.[[', '.||[[').replace('\n', '||')

    p = p.replace('.', '||.||').replace(',', '||,||').replace(';', '||;||').replace(':', '||:||').replace('?', '||?||').replace('!', '||!||')
    p = p.replace('-', '||-||').replace('/', '||/||').replace('\\', '||\\||').replace('\'\'\'', '||\'\'\'||')
    p = p.replace('(', '||(||').replace(')', '||)||')
    p = p.replace('[', '||[||').replace(']', '||]||')
    p = p.replace('{', '||{||').replace('}', '||}||')
    p = p.replace('*', '||*||').replace('#', '||#||').replace('@', '||@||').replace('&', '||&||')
    p = p.replace('=', '||=||').replace('+', '||+||').replace('_', '||_||').replace('%', '||%||')
    p = p.replace('~', '||~||')
    p = p.replace('$', '||$||')
    p = p.replace('^', '||^||')

    p = p.replace('<', '||<||').replace('>', '||>||')
    p = p.replace('[||||[', '[[').replace(']||||]', ']]')
    p = p.replace('{||||{', '{{').replace('}||||}', '}}')
    p = p.replace('||.||||.||||.||', '...').replace('/||||>', '/>').replace('<||||/', '</')
    p = p.replace('-||||-', '--')

    p = p.replace('<||||!||||--||', '||<!--||').replace('||--||||>', '||-->||')
    p = p.replace(' ', '||')
    while '||||' in p :
        p = p.replace('||||', '||')
    #print 'despues de'
    #print p
    
    words = filter(lambda a : a != '', p.split('||'))
    #print 'antes del despues de'
    #print words
    
    words = ['|' if w == '@' else w for w in words]
    #print 'despues del despues de'
    words = [elimina_tildes(str(x)) for x in words]
    return words
    

def computeAvgWordFreq(text_list, revision_id=0):
    
    d = {}
    
    for elem in text_list:
        if not (d.has_key(elem)):
            d.update({elem : text_list.count(elem)})
    
    if ('<' in d):        
        del d['<']
    
    if ('>' in d):
        del d['>']
        
    if ('tr' in d):
        del d['tr']
    
    if ('td' in d):
        del d['td']
            
#    if ('(' in d):
#        del d['(']
#        
#    if (')' in d):
#        del d[')']
        
    if ('[' in d):
        del d['[']
        
    if (']' in d):
        del d[']']
        
    if ('"' in d):
        del d['"']
        
#    if ('|' in d):
#        del d['|']
    
    if ('*' in d):
        del d['*']

    if ('==' in d):
        del d['==']

    
    if (len(d) > 0):                 
        return sum(d.values())/float(len(d))
    else:
        return 0
