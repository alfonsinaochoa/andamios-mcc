'''
Created on Feb 20, 2013

@author: maribelacosta
'''

class Revision(object):
    '''
    classdocs
    '''


    def __init__(self):
        self.id = 0                  # Fake sequential id. Starts in 0.
        self.revision_id = 0        # Wikipedia revision id.
        self.contributor_id = 0;     # Id of the contributor who performed the revision.
        self.contributor_name = ''   # Name of the contributor who performed the revision.
        self.contributor_ip = ''     # IP address of the contributor who performed the revision.
        self.paragraphs = {}         # Dictionary of paragraphs. It is of the form {paragraph_hash : [Paragraph]}.
        self.ordered_paragraphs = [] # Ordered paragraph hash.
        self.length = 0              # Content length (bytes).
        self.content = ''            #TODO: this should be removed. Just for debugging process.
        self.ordered_content = []    #TODO: this should be removed. Just for debugging process.
        self.total_tokens = 0        # Number of tokens in the revision.
        self.timestamp = 0
        self.lexical_diversity = 0  # lexical diversity
        
    def __repr__(self):
        return str(id(self))
    
    def to_dict(self):
        revision = {}
        #json_revision.update({'id' : revisions[revision].revision_id})
        #revision.update({'author' : {'id' : self.contributor_id, 'name' : self.contributor_name}})
        #json_revision.update({'length' : revisions[revision].length})
        #json_revision.update({'paragraphs' : revisions[revision].ordered_paragraphs})
        revision.update({'obj' : []})
        for paragraph_hash in self.ordered_paragraphs:
            p = []
            for paragraph in self.paragraphs[paragraph_hash]:
                p.append(repr(paragraph))
            revision['obj'].append(p)
            
        return revision

    def get_contributors(self):
        authors = {}

        for hash_paragraph in self.ordered_paragraphs:
            para = self.paragraphs[hash_paragraph]
            paragraph = para[-1]
            
            for hash_sentence in paragraph.ordered_sentences:
                sentence = paragraph.sentences[hash_sentence][-1]

                for word in sentence.words:
                    authors[word.author_id] = 1
                    
        return list(authors)


    def get_content_authors(self):
        text = []
        authors = []
        words_par = []
        sentences_par = []
        chars_par = []
        num_par = 0
        num_sen = 0
        

        for hash_paragraph in self.ordered_paragraphs:
            para = self.paragraphs[hash_paragraph]
            paragraph = para[-1]
            num_sentences = 0
            num_words = 0
            num_chars = 0
            num_par +=1
            
            for hash_sentence in paragraph.ordered_sentences:
                sentence = paragraph.sentences[hash_sentence][-1]
                num_sentences += 1
                num_sen+=1
                
                for word in sentence.words:
                    text.append(word.value)
                    authors.append(word.author_id)
                    num_words += 1
                    num_chars += len(word.value)
        
            words_par.append(num_words)
            sentences_par.append(num_sentences)
            chars_par.append(num_chars)
        
        return text, authors, num_par, num_sen, sentences_par, words_par, chars_par
    