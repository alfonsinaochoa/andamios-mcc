# -*- coding: utf-8 -*-
import sys
import json
reload(sys)
sys.setdefaultencoding('utf-8')  # @UndefinedVariable

import numpy as np
from wikiWho.wmf import dump
from os import listdir
from os.path import isfile, join
from wikiWho.structures import Text
from nlp.topics_extractor_nmf import TopicsExtractorNMF
from nlp.topics_extractor_lda import TopicsExtractorLDA
from pymongo import MongoClient
from nlp.stop_words_handler import * 
from nlp.stemmer_handler import *
from nltk.tokenize import RegexpTokenizer
from nltk.stem import SnowballStemmer
from settings import *
import nltk

def extract_topics_content(topics):
	matches = []
	for x in topics:
		key_orig = str(x[0])
		val_orig = x[1]
		band = False
		#print 'key orig',key_orig
		for y in topics:
			key = str(y[0])
			val = y[1]
			if (key_orig != key) and (key_orig.lower() in key.lower()):
				val = val + val_orig
				tuple_match = (key,val)
				tst = dict(matches)
				search_key = [key for key, value in tst.items() if key_orig in key.lower()]
				if len(search_key)==0 and band is False:
					band = True
			 		#print 'add1',tuple_match
					matches.append(tuple_match)
					topics.remove(y)
			elif (key_orig != key) and (key.lower() in key_orig.lower()):
				val = val + val_orig
				tuple_match = (key_orig,val)
				tst = dict(matches)
				search_key = [key for key, value in tst.items() if key_orig in key.lower()]
				if len(search_key)==0 and band is False:
					band = True
					#print 'add2',tuple_match
					matches.append(tuple_match)
					topics.remove(y)
					#print a
		if band is False:
			tst = dict(matches)
			search_key = [key for key, value in tst.items() if key_orig in key.lower()]
			if len(search_key)==0:
				tuple_match = (key_orig,val_orig)
				#print 'add3',tuple_match	
				matches.append(tuple_match)
			else:
				key = search_key[0]
				#print 'key find',search_key,key_orig,val_orig
				for match in matches:
					#match = list(match)
					if key in match[0]:
						#print 'aumentar'
						#match[1] = match[1] + 22
						match = tuple(match)
		else:
			topics.remove(x)	
			
	return matches

def get_topics(text):
	paragraphs_with_prepro = []
	n_words = 0
	paragraphs = Text.splitIntoParagraphs(text)
	#print len(paragraphs)
        band = False
    	for paragraph in paragraphs:
		paragraph = paragraph.lower()
		#print paragraph
		sentences = Text.splitIntoSentences(paragraph)
		first_sentence = sentences[0].strip('\n')
		if first_sentence.startswith('==') and first_sentence.endswith('==') and (first_sentence.find("obra")!=-1 or first_sentence.find("enlaces externos")!=-1 or first_sentence.find("bibliograf")!=-1 or first_sentence.find("referencias")!=-1 or first_sentence.find("notas")!=-1 or first_sentence.find("ase tambi")!=-1):
			#print first_sentence
			band=True
		elif band==True:
			band=False
		else:
			text_sentences = " ".join(sentences)
			#print "----------------------"
			#print " Tokenizer "
			#toker = RegexpTokenizer(r'\w+|\S+')
			#words = toker.tokenize(text_sentences)
			#print text_sentences
			#toker = RegexpTokenizer(r'[a-zA-ZñÑáéíóúüÁÉÍÓÚÜ\s]+')
			#words = toker.tokenize(text_sentences)
			#spanish_tokenizer = nltk.data.load('tokenizers/punkt/spanish.pickle')
			#print words
			#words2 = spanish_tokenizer.tokenize(text_sentences)
			#print words2# -*- coding: utf-8 -*-
			words = Text.splitIntoWords(text_sentences)
			#print words3
			#print "Total de palabras: "+str(len(words))
			#print "----------------------"
			#print " STOP WORDS "
			stopwords = nltk.corpus.stopwords.words('spanish')
			stopwords2 = nltk.corpus.stopwords.words('english')
			stopwords_extras = ['aunque','aun','etc','luego','do','solo','ref','url','http','redireccion','html','mientras','redirect','habia','habiamos','align','style','color','flag','image','name','web','mode','height','heights','mismo','cuyo','group']
			words_with_stop = []
			for word in words:
				if (word not in stopwords2 and word not in stopwords and word.isalpha() and word not in stopwords_extras and len(word)>2) or word=='.':
					words_with_stop.append(word)
			#print "Total de palabras(with stopwords): "+str(len(words_with_stop))
			#print " STEMMING "
			#print words_with_stop
			words_stemming = [None] * len(words_with_stop)
			for index, word in enumerate(words_with_stop):
				word = word.encode("utf-8")
				word = ''.join(word).lower()
				sqlProceso = "select palabra,etiquetas,opciones from corpus where lower(opciones) like '%,"+word+"' or lower(opciones) like '"+word+",%' or lower(opciones)='"+word+"' or lower(opciones) like '%,"+word+",%';"
				#print sqlProceso
				cursor.execute(sqlProceso)
				try:        
					curso1 = cursor.fetchall()
				except:
					curso1 = []
				if len(curso1)==0 :
					words_stemming[index] = word
				else:
					if word=='.':
						words_stemming[index] = word
					else:
						corpus = str(curso1[0][0])
						opciones = str(curso1[0][2])
						etiquetas = str(curso1[0][1])
						#print etiquetas.find(",")
						if etiquetas.find(",")!=-1:
							#print word
							#print opciones
							opc = opciones.split(",")
							eti = etiquetas.split(",")
							#print opc
							idx = opc.index(word)
							#print idx
							try:
								etiqueta = eti[idx]
							except:
								print "Fuera de rango"
								etiqueta = eti[0]
						else:
							etiqueta = etiquetas
						#print opciones
						#print etiqueta
						#print corpus
						#if etiqueta[0]!='V':
						words_stemming[index] = word
			words_stemming = [x for x in words_stemming if x is not None]
			if len(words_stemming)>n_words:
				text_words_with_stop = " ".join(words_stemming)
				paragraphs_with_prepro.append(text_words_with_stop.encode("utf-8"))
	topics_words,topics_words_lda = extract_topics(paragraphs_with_prepro)
	topics_words = sorted(topics_words.items(), key=lambda x: x[1], reverse=True)
	topics_words_lda = sorted(topics_words_lda.items(), key=lambda x: x[1], reverse=True)
	return topics_words,topics_words_lda

def extract_topics(paragraphs):
	# TODO: hyperparameters tunning
	num_topics = len(paragraphs)
	num_words = 5
	nmf_model = TopicsExtractorNMF(n_topics=num_topics, n_top_words=num_words)
	lda_model = TopicsExtractorLDA(n_topics=num_topics, n_top_words=num_words)
	topics_words = nmf_model.extract_topics(paragraphs)
	topics_words_lda = lda_model.extract_topics(paragraphs)
	return topics_words,topics_words_lda

client = MongoClient('localhost', 27017)
db = client.amd
url = 'datos4.xml'
dumpIterator = dump.Iterator(url)
for page in dumpIterator.readPages():
	print page.id + " -" + page.title
	for revision in page.readRevisions():
		title = page.title
		text = revision.getText()
		topics_nmf,topics_lda = get_topics(text)
		topics_nmf_order = sorted(topics_nmf, key=lambda x: x[1], reverse=True)
		topics_nmf1 = extract_topics_content(topics_nmf)
		topics_nmf1_order = sorted(topics_nmf1, key=lambda x: x[1], reverse=True)
		topics_lda_order = sorted(topics_lda, key=lambda x: x[1], reverse=True)
		topics_lda1 = extract_topics_content(topics_lda)
		topics_lda1_order = sorted(topics_lda1, key=lambda x: x[1], reverse=True)
		print "NMF"
		print topics_nmf_order[:10]
		print topics_nmf1_order[:10]
		print "LDA"
		print topics_lda_order[:10]
		print topics_lda1_order[:10]
		print "____________________"
		#for paragraph in paragraphs:
		#	paragraph = paragraph.strip()
		#	sentences = Text.splitIntoSentences(paragraph)
		#	for sentence in sentences:
		#		words = Text.splitIntoWords(paragraph)
		#	fd
		#documents = db.pages.find({'title' : title})
		#if documents.count()==0:
			#result = db.pages.insert_one({"title": title , "text": text})
			#print result.inserted_id



