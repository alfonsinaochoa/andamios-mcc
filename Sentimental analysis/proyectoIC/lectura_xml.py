# coding: utf-8
import re, string
import csv
import unicodedata
import nltk.classify
import svm

from svmutil import *
from settings import *
from tokenize import *
from xml.dom import minidom
from nltk.corpus import stopwords

	
def smart_find(haystack, needle):
	if haystack.startswith(needle+" "):
	    return True
	if haystack.endswith(" "+needle):
	    return True
	if haystack.find(" "+needle+" ") != -1:
	    return True
	return False

def get_tweet_polarity(actual_polarity, polarity):	
	if actual_polarity == polarity:
		return actual_polarity
	if (actual_polarity == 'P' and polarity == 'NEU') or (actual_polarity == 'NEU' and polarity == 'P'):
		return 'P'
	if (actual_polarity == 'N' and polarity == 'NEU') or (actual_polarity == 'NEU' and polarity == 'N'):
		return 'N'
	if (actual_polarity == 'P' and polarity == 'N') or (actual_polarity == 'N' and polarity == 'P'):
		return 'NEU'

def replace_accent_marks(string_to_replace):	
	string_to_replace = ''.join((c for c in unicodedata.normalize('NFD', string_to_replace) if unicodedata.category(c) != 'Mn'))
	return string_to_replace

 
def searchFeatureReduction(string_to_replace, dic_tweets):
	
	#Convert to lower case
	string_to_replace = string_to_replace.lower()
	#Convert www.* or https?://* to URL
	if re.search('((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))', string_to_replace) != None:
		if not(dic_tweets.has_key('URL')):
			dic_tweets['URL'] = {'tipo':'URL','amount':1}				
		else:
			dic_tweets.get('URL')['amount'] += 1
	
	#Convert @username to USUARIO		
	if re.search('@[^\s]+', string_to_replace) != None:
		if not(dic_tweets.has_key('USUARIO')):
			dic_tweets['USUARIO'] = {'tipo':'USUARIO','amount':1}				
		else:
			dic_tweets.get('USUARIO')['amount'] += 1  
	
	#Replace #word with word	
	if re.search(r'#([^\s]+)', string_to_replace) != None:
		if not(dic_tweets.has_key('WORD')):
			dic_tweets['WORD'] = {'tipo':'WORD','amount':1}				
		else:
			dic_tweets.get('WORD')['amount'] += 1

	#Replace emoticons	
	#if smart_find(string_to_replace, ':(') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1
	#if smart_find(string_to_replace, ':-(') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1
	#if smart_find(string_to_replace, ': (') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1
	#if smart_find( string_to_replace, ';-(') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1	
	#if smart_find( string_to_replace, ':)') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1
	#if smart_find( string_to_replace, ':-)') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1
	#if smart_find( string_to_replace, ': )') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1			
	#if smart_find( string_to_replace, ':D') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1
	#if smart_find(string_to_replace, '=)') != False:
	#	if not(dic_tweets.has_key('EMOTICONS')):
	#		dic_tweets['EMOTICONS'] = {'tipo':'EMOTICONS','amount':1}				
	#	else:
	#		dic_tweets.get('EMOTICONS')['amount'] += 1	
	return dic_tweets

def processTweet(string_to_replace):
	#Convert to lower case
	string_to_replace = string_to_replace.lower()
	#Convert www.* or https?://* to URL
	string_to_replace = re.sub('((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))','URL',string_to_replace)
	#Convert @username to USUARIO
	string_to_replace = re.sub('@[^\s]+','USUARIO',string_to_replace)
	#Remove additional white spaces
	string_to_replace = re.sub('[\s]+', ' ', string_to_replace)
	#Replace #word with word
	string_to_replace = re.sub(r'#([^\s]+)', r'\1', string_to_replace)
	#trim
	string_to_replace = string_to_replace.strip('\'"')
	
	#if string_to_replace.startswith("@"):
	#	string_to_replace = string_to_replace.replace(string_to_replace,"USUARIO")
	#if string_to_replace.startswith("http://") or string_to_replace.startswith("https://"):
	#	string_to_replace = string_to_replace.replace(string_to_replace,"URL")
	#if string_to_replace.find("@") != -1:
	#	string_to_replace = string_to_replace.replace(string_to_replace,"CORREO")
	return string_to_replace


def replaceTwoOrMore(s):
	#look for 2 or more repetitions of character and replace with the character itself
	pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
	return pattern.sub(r"\1\1", s)


def getStopWordList():	
	#read the stopwords file and build a list
	stopWords = []
	stopWords.append('USUARIO')
	stopWords.append('URL')
	stopWords.append('CORREO')
	
	fp = open(dictionary, 'r')
	line = fp.readline()
	while line:
	    word = line.strip()
	    stopWords.append(word)
	    line = fp.readline()
	fp.close()
	return stopWords

def getFeatureVector(tweet, stopWords):	
	featureVector = []
	for w in simpleTokenize(tweet):
		#print "for w:" + w
		#replace two or more with two occurrences
		w = replaceTwoOrMore(w)		
		#strip punctuation
		w = w.strip('\'"?,.')
		#check if the word stats with an alphabet
		val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", w)
		#ignore if it is a stop word
		if(w in stopWords or val is None):
			continue
		else:
			featureVector.append(w.lower())
	return featureVector

	
def extract_features(tweet):
	tweet_words = set(tweet)
	features = {}
	for word in featureList:
		features['contains(%s)' % word] = (word in tweet_words)
	return features

def getSVMFeatureVectorandLabels(tweets, featureList):
    sortedFeatures = sorted(featureList)
    map = {}
    feature_vector = []
    labels = []
    for t in tweets:
        label = 0
        map = {}
        #Initialize empty map
        for w in sortedFeatures:
            map[w] = 0

        tweet_words = t[0]
        tweet_opinion = t[1]
        #Fill the map
        for word in tweet_words:
            #process the word (remove repetitions and punctuations)
            word = replaceTwoOrMore(word)
            word = word.strip('\'"?,.')
            #set map[word] to 1 if word exists
            if word in map:
                map[word] = 1
        #end for loop
        values = map.values()
        feature_vector.append(values)
        if(tweet_opinion == 'P'):
            label = 0
        elif(tweet_opinion == 'N'):
            label = 1
        elif(tweet_opinion == 'NEU'):
            label = 2
        labels.append(label)
    #return the list of feature_vector and labels
    return {'feature_vector' : feature_vector, 'labels': labels}

def getSVMFeatureVector(tweets, featureList):
        sortedFeatures = sorted(featureList)
        map = {}
        feature_vector = []
        for t in tweets:
            label = 0
            map = {}
            #Initialize empty map
            for w in sortedFeatures:
                map[w] = 0
            #Fill the map
            for word in t:
                if word in map:
                    map[word] = 1
            #end for loop
            values = map.values()
            feature_vector.append(values)                    
        return feature_vector
    
    
def preprocess_tweets():
	dic_tweets = dict()
	
	training_set_file = open(training_set, "wb")
	test_set_file = open(test_set, "wb")
	numero_lineas = 1
	for x in os.listdir(data):
		dic_tweets.clear()
		doc = minidom.parse(data+str(x))	
		tweets = doc.getElementsByTagName("tweet")	
		for tw in tweets:
			tweetid = tw.getElementsByTagName("tweetid")[0]			
			content = tw.getElementsByTagName("content")[0]
			sentiments = tw.getElementsByTagName("sentiments")[0]
			total_polarity = ''
			string_to_replace = ''
			bandera = False
			texts = []
			for st in sentiments.getElementsByTagName("polarity"):
				polarity = st.getElementsByTagName("value")[0]
				if polarity.firstChild.data != 'NONE':
					if not smart_find(content.firstChild.data,'RT'):
						polarity_value = polarity.firstChild.data
						if polarity_value == 'P+':
							polarity_value = 'P'
						if polarity_value == 'N+':
							polarity_value = 'N'
						if total_polarity != '':
							total_polarity = get_tweet_polarity(total_polarity, polarity_value)
						else:
							total_polarity = polarity_value
						bandera = True						
			if bandera == True:				
				string_to_replace = replace_accent_marks(content.firstChild.data.lower())
				texts = ([word for word in string_to_replace.lower().split()])
				string_to_replace = ''
				for s in texts:
					dic_tweets = searchFeatureReduction(s, dic_tweets)
					string_to_replace = string_to_replace + ' ' + s #processTweet(s)s
				
				if numero_lineas <= training_number:
					if numero_lineas == training_number:
						training_set_file.write("|" + str(total_polarity) + "|,|" + string_to_replace + "|")
					else:
						training_set_file.write("|" + str(total_polarity) + "|,|" + string_to_replace + "|\n")
				else:
					test_set_file.write("|" + str(total_polarity) + "|,|" + string_to_replace + "|\n")
				numero_lineas += 1
				#if not(dic_tweets.has_key(tweetid.firstChild.data)):
				#	dic_tweets[tweetid.firstChild.data] = {'polarity':total_polarity,'amount':1}				
				#else:
				#	dic_tweets.get(tweetid.firstChild.data)['amount'] += 1                
					
	training_set_file.close()
	test_set_file.close()
	print dic_tweets


if __name__ == "__main__":

	preprocess_tweets();

	#Read the tweets one by one and process it
	#print 'Get Feature Extraction...' 
	#inpTweets = csv.reader(open(training_set, 'rb'), delimiter=',', quotechar='|')
	#stopWords = getStopWordList()
	#count = 0;
	#featureList = []
	#tweets = []
	#for row in inpTweets:
	#    sentiment = row[0]
	#    tweet = row[1]
	#    processedTweet = processTweet(tweet)
	#    featureVector = getFeatureVector(processedTweet, stopWords)
	#    featureList.extend(featureVector)
	#    tweets.append((featureVector, sentiment));
	##end loop
	#
	## Remove featureList duplicates
	#featureList = list(set(featureList))
	##print featureList
	## Generate the training set
	#training_set = nltk.classify.util.apply_features(extract_features, tweets)
	#
	## Train the Naive Bayes classifier
	#NBClassifier = nltk.NaiveBayesClassifier.train(training_set)
	#
	## Test the classifier
	#results_file = open(NaiveBayesClassifier_results, "wb")
	#testTweets = csv.reader(open(test_set, 'rb'), delimiter=',', quotechar='|')
	#for row in testTweets:
	#	testTweet = row[1]
	#	processedTestTweet = processTweet(testTweet)
	#	sentiment = NBClassifier.classify(extract_features(getFeatureVector(processedTestTweet, stopWords)))
	#	#print "testTweet = %s, sentiment = %s\n" % (testTweet, sentiment)
	#	results_file.write("|" + str(sentiment) + "|,|" + testTweet + "|\n")
	#results_file.close()
	##print NBClassifier.show_most_informative_features(100)
	##print extract_features(processedTestTweet.split())
	#
	##Train the classifier
	#result = getSVMFeatureVectorandLabels(tweets, featureList)
	##print result
	#problem = svm_problem(result['labels'], result['feature_vector'])
	#
	##'-q' option suppress console output
	#param = svm_parameter()
	#param.kernel_type = LINEAR
	#classifier = svm_train(problem, param)
	#svm_save_model('classifierTweets', classifier)
	#
	##Test the classifier
	#SVM_results_file = open(SVM_results, "wb")
	#for row in testTweets:
	#	testTweet = row[1]
	#	processedTestTweet = processTweet(testTweet)
	#	print processedTestTweet
	#	test_feature_vector = getSVMFeatureVector(getFeatureVector(processedTestTweet, stopWords), featureList)
	#	#p_labels contains the final labeling result
	#	p_labels, p_accs, p_vals = svm_predict([0] * len(test_feature_vector),test_feature_vector, classifier)
	#	print "labels:"
	#	print p_labels
	#	print "accuracy:"
	#	print p_accs
	#	print "values:"
	#	print p_vals
	#	SVM_results_file.write("|" + str(p_labels) + "|,|" + str(p_accs) + "|,|" + str(p_vals) + "|\n")
	#SVM_results_file.close()