# coding: utf-8
import re, string
import csv
import unicodedata
import nltk.classify
import svm

from svmutil import *
from settings import *
from tokenize import *
from xml.dom import minidom
from nltk.corpus import stopwords

	
def smart_find(haystack, needle):
	if haystack.startswith(needle+" "):
	    return True
	if haystack.endswith(" "+needle):
	    return True
	if haystack.find(" "+needle+" ") != -1:
	    return True
	return False

def get_tweet_polarity(actual_polarity, polarity):	
	if actual_polarity == polarity:
		return actual_polarity
	if (actual_polarity == 'P' and polarity == 'NEU') or (actual_polarity == 'NEU' and polarity == 'P'):
		return 'P'
	if (actual_polarity == 'N' and polarity == 'NEU') or (actual_polarity == 'NEU' and polarity == 'N'):
		return 'N'
	if (actual_polarity == 'P' and polarity == 'N') or (actual_polarity == 'N' and polarity == 'P'):
		return 'NEU'

def replace_accent_marks(string_to_replace):	
	string_to_replace = ''.join((c for c in unicodedata.normalize('NFD', string_to_replace) if unicodedata.category(c) != 'Mn'))
	return string_to_replace

def processTweet(string_to_replace):
	#Convert to lower case
	string_to_replace = string_to_replace.lower()
	#Convert www.* or https?://* to URL
	string_to_replace = re.sub('((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))','URL',string_to_replace)
	#Convert @username to USUARIO
	string_to_replace = re.sub('@[^\s]+','USUARIO',string_to_replace)
	#Remove additional white spaces
	string_to_replace = re.sub('[\s]+', ' ', string_to_replace)
	#Replace #word with word
	string_to_replace = re.sub(r'#([^\s]+)', r'\1', string_to_replace)
	#trim
	string_to_replace = string_to_replace.strip('\'"')
	
	#if string_to_replace.startswith("@"):
	#	string_to_replace = string_to_replace.replace(string_to_replace,"USUARIO")
	#if string_to_replace.startswith("http://") or string_to_replace.startswith("https://"):
	#	string_to_replace = string_to_replace.replace(string_to_replace,"URL")
	#if string_to_replace.find("@") != -1:
	#	string_to_replace = string_to_replace.replace(string_to_replace,"CORREO")
	return string_to_replace


def replaceTwoOrMore(s):
	#look for 2 or more repetitions of character and replace with the character itself
	pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
	return pattern.sub(r"\1\1", s)


def getStopWordList():	
	#read the stopwords file and build a list
	stopWords = []
	stopWords.append('USUARIO')
	stopWords.append('URL')
	stopWords.append('CORREO')
	
	fp = open(dictionary, 'r')
	line = fp.readline()
	while line:
	    word = line.strip()
	    stopWords.append(word)
	    line = fp.readline()
	fp.close()
	return stopWords

def getFeatureVector(tweet, stopWords):	
	#featureVector = []
	featureVector = ''
	for w in simpleTokenize(tweet):
		#print "for w:" + w
		#replace two or more with two occurrences
		w = replaceTwoOrMore(w)		
		#strip punctuation
		w = w.strip('\'"?,.')
		#check if the word stats with an alphabet
		val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", w)
		#ignore if it is a stop word
		if(w in stopWords or val is None):
			continue
		else:
			featureVector = featureVector + ' ' + w
			#featureVector.append(w.lower())
	return featureVector

	
def extract_features(tweet):
	tweet_words = set(tweet)
	features = {}
	for word in featureList:
		features['contains(%s)' % word] = (word in tweet_words)
	return features

    
def preprocess_tweets():	
	print 'Pre processing tweets...' 
	positive_training_set_file = open(positive_training_set, "wb")
	negative_training_set_file = open(negative_training_set, "wb")
	neutral_training_set_file = open(neutral_training_set, "wb")
	stopWords = getStopWordList()
	numero_lineas = 1
	for x in os.listdir(data):		
		doc = minidom.parse(data+str(x))	
		tweets = doc.getElementsByTagName("tweet")	
		for tw in tweets:
			tweetid = tw.getElementsByTagName("tweetid")[0]			
			content = tw.getElementsByTagName("content")[0]
			sentiments = tw.getElementsByTagName("sentiments")[0]
			total_polarity = ''
			string_to_replace = ''
			bandera = False
			texts = []
			line = ''
			for st in sentiments.getElementsByTagName("polarity"):
				polarity = st.getElementsByTagName("value")[0]
				if polarity.firstChild.data != 'NONE':
					if not smart_find(content.firstChild.data,'RT'):
						polarity_value = polarity.firstChild.data
						if polarity_value == 'P+':
							polarity_value = 'P'
						if polarity_value == 'N+':
							polarity_value = 'N'
						if total_polarity != '':
							total_polarity = get_tweet_polarity(total_polarity, polarity_value)
						else:
							total_polarity = polarity_value
						bandera = True						
			if bandera == True:				
				string_to_replace = replace_accent_marks(content.firstChild.data.lower())
				texts = ([word for word in string_to_replace.lower().split()])
				string_to_replace = ''
				for s in texts:
					string_to_replace = string_to_replace + ' ' + processTweet(s)
				#print "string_to_replace:" + string_to_replace + "\n Feature vector:"
				featureVector = getFeatureVector(string_to_replace, stopWords)
				#print featureVector
				line = featureVector + "\n"						
				if total_polarity == 'P':
					positive_training_set_file.write(line)
				elif total_polarity == 'N':
					negative_training_set_file.write(line)
				else:
					neutral_training_set_file.write(line)							       
					
	positive_training_set_file.close()
	negative_training_set_file.close()
	neutral_training_set_file.close()
	

if __name__ == "__main__":	
	preprocess_tweets();	